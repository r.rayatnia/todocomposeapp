package com.me.todoapp.data

import com.me.todoapp.models.Task
import kotlinx.coroutines.flow.Flow

class TaskRepositoryImpl(
    api : TaskApi,
    dao : TaskDao
) : TaskRepository {

    override suspend fun getTask(taskId: Int): Task {
        TODO("Not yet implemented")
    }

    override suspend fun getTasks(): List<Task> {
        TODO("Not yet implemented")
    }

    override suspend fun insertTask(task: Task) {
        TODO("Not yet implemented")
    }

    override suspend fun insertTasks(tasks: List<Task>) {
        TODO("Not yet implemented")
    }

    override suspend fun updateTasks(task: Task) {
        TODO("Not yet implemented")
    }
}