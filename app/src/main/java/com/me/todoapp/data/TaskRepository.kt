package com.me.todoapp.data

import com.me.todoapp.models.Task
import kotlinx.coroutines.flow.Flow

interface TaskRepository {

    suspend fun getTask(taskId: Int): Task
    suspend fun getTasks(): List<Task>

    suspend fun insertTask( task: Task)
    suspend fun insertTasks( tasks: List<Task>)

    suspend fun updateTasks(task: Task)

}