package com.me.todoapp.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.me.todoapp.models.TaskEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface TaskDao {

    @Query("SELECT * FROM task")
    fun getAll() : Flow<List<TaskEntity>>

    @Query("SELECT * FROM task WHERE task.title like :title")
    fun get( title: String): List<TaskEntity>

    @Insert
    fun save( task: TaskEntity)

    @Insert
    fun save( tasks: List<TaskEntity>)

    @Delete
    fun delete(task: TaskEntity)

    @Update
    fun update( task: TaskEntity)
}