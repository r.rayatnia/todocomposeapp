package com.me.todoapp.data

class TaskApi {

    suspend fun getAll(){}
    //get()

    suspend fun save(){}
    suspend fun saveAll(){}

    suspend fun update(){}

    suspend fun delete(){}
}