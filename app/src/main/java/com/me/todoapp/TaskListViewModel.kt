package com.me.todoapp

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.me.todoapp.data.TaskRepository
import com.me.todoapp.models.Task
import kotlinx.coroutines.flow.MutableStateFlow

class TaskListViewModel(
    private val repository: TaskRepository,
) : ViewModel() {

    private val _tasks = mutableStateListOf<Task>()
    val tasks : List<Task>
        get() = _tasks



    class Factory(private val repository: TaskRepository) : ViewModelProvider.Factory{
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return TaskListViewModel( repository ) as T
        }
    }

}