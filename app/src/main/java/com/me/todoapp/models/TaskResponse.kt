package com.me.todoapp.models

@Serializable
data class TaskResponse(
    val id: Int,
    val title : String
)