package com.me.todoapp.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "task")
class TaskEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String
)