package com.me.todoapp.app

import android.app.Application
import androidx.room.Room
import com.me.todoapp.data.TaskRepository
import com.me.todoapp.data.TaskRepositoryImpl

class AppContainer : Application() {

    lateinit var taskRepository: TaskRepository

    override fun onCreate() {
        super.onCreate()
        val db =
            Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java, "task-database"
            ).build()

        taskRepository = TaskRepositoryImpl(api = , dao = db.taskDao())
    }

}