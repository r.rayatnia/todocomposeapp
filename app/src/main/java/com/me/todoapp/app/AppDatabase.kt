package com.me.todoapp.app

import androidx.room.Database
import androidx.room.RoomDatabase
import com.me.todoapp.data.TaskDao
import com.me.todoapp.models.TaskEntity

@Database( entities = [TaskEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun taskDao() : TaskDao
}