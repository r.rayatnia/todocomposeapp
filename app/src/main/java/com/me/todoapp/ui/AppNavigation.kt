package com.me.todoapp.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.me.todoapp.ui.compose.TaskList
import com.me.todoapp.ui.compose.TodoDetail


object AppRoute {
    const val Home = "taskList"
    const val TaskDetail = "taskDetail/{taskId}"
    const val AddTask = "taskDetail"
    val getTaskDetailRoute : (taskId: Int) -> String = {"taskDetail/${it}"}
}

@Composable
fun AppNavHost(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    startDestination: String = AppRoute.Home
) {
    NavHost(navController = navController, startDestination = "taskList" ) {
        composable(AppRoute.AddTask){ TodoDetail() }
        composable(AppRoute.TaskDetail){
            val taskId = it.arguments?.getInt("taskId")
            TodoDetail( taskId = taskId)
        }
        composable(AppRoute.Home){
            TaskList(
                onTaskItemClicked = { task -> navController.navigate( AppRoute.getTaskDetailRoute(task.id))},
                onAddNewTaskClicked = { navController.navigate(AppRoute.AddTask)}
            )
        }
    }
}