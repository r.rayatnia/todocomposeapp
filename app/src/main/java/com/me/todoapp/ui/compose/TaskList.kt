package com.me.todoapp.ui.compose

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import com.me.todoapp.TaskListViewModel
import com.me.todoapp.data.TaskRepository
import com.me.todoapp.models.Task

@Composable
fun TaskList(
    modifier: Modifier = Modifier,
    onTaskItemClicked : (Task)->Unit,
    onAddNewTaskClicked : ()->Unit) {
    val repository : TaskRepository = AppContainer.taskRepository
    val taskViewModel : TaskListViewModel = viewModel( factory = TaskListViewModel.Factory(repository) )
    val tasks = remember { taskViewModel.tasks }
    //TODO( "what is lazyListState" )
    LazyColumn(modifier = modifier.fillMaxSize()) {
        items(tasks) { data->
            Button( onClick = { onTaskItemClicked(data) }, modifier = modifier ) {
                Text( text = data.title, modifier = modifier )
            }
        }
    }


}